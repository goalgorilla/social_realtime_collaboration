/**
 * @file
 * CKEditor 5 implementation of {@link Drupal.editors} API.
 */


(function($, Drupal, debounce, drupalSettings, ClassicEditor) {

  /**
   * @namespace
   */
  Drupal.editors.ckeditor5 = {
    /**
     * Editor attach callback.
     *
     * @param {HTMLElement} element
     *   The element to attach the editor to.
     * @param {string} format
     *   The text format for the editor.
     *
     * @return {bool}
     *   Whether attaching a `<textarea>` CKEditor 5 instance succeeded.
     */
    attach(element, format) {
      var toolbar4 = format.editorSettings.toolbar;
      var toolbar5 = [];
      var group, item;
      var comments = false, suggestions = false;

      for (group of toolbar4) {
        if (group.items !== undefined) {
          var first = true;

          for (item of group.items) {
            if (item.length > 0) {
              if (item === 'comment') {
                comments = true;
              }
              else if (item === 'trackChanges') {
                suggestions = true;
              }

              if (first && toolbar5.length > 0) {
                toolbar5.push('|');
              }

              toolbar5.push(item);

              first = false;
            }
          }
        }
      }

      var options = [
        {
          model: 'paragraph',
          title: 'Paragraph Format',
          class: 'ck-heading_paragraph'
        },
        {
          model: 'tag_div',
          view: 'div',
          title: 'Normal',
          class: 'ck-heading_div'
        }
      ];

      for (var level = 1; level <= 6; level++) {
        options.push({
          model: 'heading' + level,
          view: 'h' + level,
          title: 'Heading ' + level,
          class: 'ck-heading_heading' + level
        });
      }

      options.push({
        model: 'tag_pre',
        view: 'pre',
        title: 'Formatted',
        class: 'ck-heading_pre'
      });

      const { ClassicEditor: Editor, EditorWatchdog } = ClassicEditor;

      const watchdog = new EditorWatchdog();
      window.watchdog = watchdog;

      const prefix = '#' + $(element).attr('name').replace(/(\]\[|\[|\])/g, '-');
      const sidebarElement = document.querySelector( prefix + 'comments' );
      const users = document.querySelector(prefix + 'users' );

      watchdog.setCreator( ( el, config ) => {
        return Editor.create( el, config )
          .then( editor => {
            if (comments) {
              // Switch between inline, narrow sidebar and wide sidebar according to the window size.
              const annotations = editor.plugins.get('Annotations');

              function refreshDisplayMode() {
                if (window.innerWidth < 900) {
                  sidebarElement.classList.remove('narrow');
                  sidebarElement.classList.add('hidden');
                  annotations.switchTo('inline');
                }
                else if (window.innerWidth < 1200) {
                  sidebarElement.classList.remove('hidden');
                  sidebarElement.classList.add('narrow');
                  annotations.switchTo('narrowSidebar');
                }
                else {
                  sidebarElement.classList.remove('hidden', 'narrow');
                  annotations.switchTo('wideSidebar');
                }
              }

              // Prevent closing the tab when any action is pending.
              editor.ui.view.listenTo(window, 'beforeunload', (evt, domEvt) => {
                if (editor.plugins.get('PendingActions').hasAny) {
                  domEvt.preventDefault();
                  domEvt.returnValue = true;
                }
              });

              editor.ui.view.listenTo(window, 'resize', refreshDisplayMode);
              refreshDisplayMode();
            }

            if (suggestions) {
              // Get the TrackChanges plugin from editor.
              const trackChanges = editor.plugins.get('TrackChanges')

              // Trigger event when suggestion added/updated/deleted.
              editor.model.markers.on('update:suggestion', (t, n, e) => {
                // Set suggestion ID, user ID, node ID.
                const suggestionID = t.name.split(':')[3];
                const user = t.name.split(':')[4];
                const uid = user.split('-')[1];
                const nid = editor.config._config.collaboration.channelId.split('-')[1]

                // Check if suggestion is new.
                if (e === null) {
                  sendSuggestion(suggestionID, uid, nid);
                  return;
                }

                // Check if suggestion deleted from editor.
                if (e.start.path.length === 1) {
                  sendSuggestion(suggestionID, uid, nid, true);
                }
                else {
                  let deleteSuggestion = true;
                  // Get all suggestion in editor.
                  const suggestions = trackChanges.getSuggestions();
                  $.each(suggestions, (i, suggestion) => {
                    // If suggestion exist in all suggestions do delete it.
                    if (suggestion.id === suggestionID) {
                      deleteSuggestion = false;
                      return false;
                    }
                  });
                  if (deleteSuggestion) {
                    sendSuggestion(suggestionID, uid, nid, true);
                  }
                }
              });
            }

            return editor;
          } );
      });

      watchdog.setDestructor( editor => editor.destroy() );

      var currentPath = drupalSettings.path.currentPath;
      var quickEdit = $('.quickedit-field-form');

      if (quickEdit.length !== 0) {
        // Use the same path on quickedit as for edit mode to build common
        // channelId for collaboration.
        currentPath = currentPath + '/edit';
      }

      if (currentPath.indexOf('/collaboration') >= 0) {
        currentPath = currentPath.replace('/collaboration', '/edit');
      }

      // Check if current node is a translation and add language prefix to build a unique path.
      if (typeof drupalSettings.path.currentLanguage !== 'undefined') {
        var currentLanguage = drupalSettings.path.currentLanguage;
        currentPath = currentPath.replace(/^/, currentLanguage + '/');
      }

      watchdog
        .create(element, {
          heading: {
            options: options
          },
          language: 'en',
          toolbar: toolbar5,
          cloudServices: {
            tokenUrl: drupalSettings.collaboration.url,
            uploadUrl: drupalSettings.collaboration.upload,
            webSocketUrl: drupalSettings.collaboration.websocket
          },
          collaboration: {
            channelId: currentPath.replace(/\//g, '-')
          },
          commentsOnly: comments && !suggestions,
          sidebar: {
            container: sidebarElement
          },
          presenceList: {
            container: users
          }
        });

      return true;
    },

    /**
     * Editor detach callback.
     *
     * @param {HTMLElement} element
     *   The element to detach the editor from.
     * @param {string} format
     *   The text format used for the editor.
     * @param {string} trigger
     *   The event trigger for the detach.
     *
     * @return {bool}
     *   Whether detaching the CKEditor 5 instance succeeded.
     */
    detach(element, format, trigger) {

      var data = document.querySelector('.ck-editor__editable');
      var editorInstance = data.ckeditorInstance;

      if (editorInstance) {
        if (trigger === 'serialize') {
          editorInstance.updateSourceElement();
        } else {
          editorInstance.destroy();
        }
      }

      return true;
    },

    /**
     * Reacts on a change in the editor element.
     *
     * @param {HTMLElement} element
     *   The element where the change occurred.
     * @param {function} callback
     *   Callback called with the value of the editor.
     *
     * @return {bool}
     *   Whether listening for changes on the CKEditor 5 instance succeeded.
     */
    onChange(element, callback) {

      setTimeout(function () {

        var data = document.querySelector('.ck-editor__editable');

        if (data !== null) {
          var editorInstance = data.ckeditorInstance;

          if (editorInstance) {
            $(document).click(editorInstance, debounce(function () {
              callback(editorInstance.getData());
            }, 400));
          }
        }
      }, 2000);

      return true;
    },

    /**
     * Attaches an inline editor to a DOM element.
     *
     * @param {HTMLElement} element
     *   The element to attach the editor to.
     * @param {object} format
     *   The text format used in the editor.
     * @param {string} [mainToolbarId]
     *   The id attribute for the main editor toolbar, if any.
     * @param {string} [floatedToolbarId]
     *   The id attribute for the floated editor toolbar, if any.
     *
     * @return {bool}
     *   Whether attaching an inline CKEditor 5 instance succeeded.
     */
    attachInlineEditor(element, format, mainToolbarId, floatedToolbarId) {
      return true;
    },
  };

  /**
   * Send Ajax with suggestion info.
   *
   * @param {string }sid
   *   The suggestion ID.
   * @param {string} uid
   *   User ID that added/deleted suggestion.
   * @param {string} nid
   *   Node ID where suggestion was added/deleted
   * @param {boolean} del
   *   Suggestion is need to delete.
   */
  const sendSuggestion = (sid, uid, nid, del = false) => {
    $.ajax({
      type: "POST",
      url: '/suggestion-manager',
      data: {
        sid,
        uid,
        nid,
        del,
      },
    })
  }

  // Redirect on hash change when the original hash has an associated CKEditor.
  // @todo

  // Set the CKEditor cache-busting string to the same value as Drupal.
  // @todo
})(
  jQuery,
  Drupal,
  Drupal.debounce,
  drupalSettings,
  ClassicEditor
);
