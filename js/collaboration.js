(function ($, Drupal) {
  Drupal.behaviors.collabortion = {
    attach: function () {
      var $hideContent = $('.region--complementary, #section-comments + section, .site-footer');

      if ($('.navbar-secondary a[href*="collaboration"]').hasClass('is-active')) {
        $hideContent.each(function () {
          $(this).addClass('open-quickedit');
        });
      }


    }
  };
})(jQuery, Drupal);
