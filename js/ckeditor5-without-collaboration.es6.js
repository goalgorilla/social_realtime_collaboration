/**
 * @file
 * CKEditor 5 implementation of {@link Drupal.editors} API.
 */

(function(Drupal, drupalSettings, ClassicEditor) {

  /**
   * @namespace
   */
  Drupal.editors.ckeditor5 = {
    /**
     * Editor attach callback.
     *
     * @param {HTMLElement} element
     *   The element to attach the editor to.
     * @param {string} format
     *   The text format for the editor.
     *
     * @return {bool}
     *   Whether attaching a `<textarea>` CKEditor 5 instance succeeded.
     */
    attach(element, format) {
      var toolbar4 = format.editorSettings.toolbar;
      var toolbar5 = [];
      var group, item;

      for (group of toolbar4) {
        if (group.items !== undefined) {
          var first = true;

          for (item of group.items) {
            if (item.length > 0) {
              if (first && toolbar5.length > 0) {
                toolbar5.push('|');
              }

              toolbar5.push(item);

              first = false;
            }
          }
        }
      }

      var options = [
        {
          model: 'paragraph',
          title: 'Paragraph Format',
          class: 'ck-heading_paragraph'
        },
        {
          model: 'tag_div',
          view: 'div',
          title: 'Normal',
          class: 'ck-heading_div'
        }
      ];

      for (var level = 1; level <= 6; level++) {
        options.push({
          model: 'heading' + level,
          view: 'h' + level,
          title: 'Heading ' + level,
          class: 'ck-heading_heading' + level
        });
      }

      options.push({
        model: 'tag_pre',
        view: 'pre',
        title: 'Formatted',
        class: 'ck-heading_pre'
      });

      ClassicEditor
        .create(element, {
          heading: {
            options: options
          },
          language: 'en',
          toolbar: toolbar5,
          cloudServices: {
            tokenUrl: drupalSettings.collaboration.url,
            uploadUrl: drupalSettings.collaboration.upload,
            webSocketUrl: drupalSettings.collaboration.websocket
          }
        });

      return true;
    },

    /**
     * Editor detach callback.
     *
     * @param {HTMLElement} element
     *   The element to detach the editor from.
     * @param {string} format
     *   The text format used for the editor.
     * @param {string} trigger
     *   The event trigger for the detach.
     *
     * @return {bool}
     *   Whether detaching the CKEditor 5 instance succeeded.
     */
    detach(element, format, trigger) {
      // @todo
      return false;
    },

    /**
     * Reacts on a change in the editor element.
     *
     * @param {HTMLElement} element
     *   The element where the change occurred.
     * @param {function} callback
     *   Callback called with the value of the editor.
     *
     * @return {bool}
     *   Whether listening for changes on the CKEditor 5 instance succeeded.
     */
    onChange(element, callback) {
      // @todo
      return false;
    },

    /**
     * Attaches an inline editor to a DOM element.
     *
     * @param {HTMLElement} element
     *   The element to attach the editor to.
     * @param {object} format
     *   The text format used in the editor.
     * @param {string} [mainToolbarId]
     *   The id attribute for the main editor toolbar, if any.
     * @param {string} [floatedToolbarId]
     *   The id attribute for the floated editor toolbar, if any.
     *
     * @return {bool}
     *   Whether attaching an inline CKEditor 5 instance succeeded.
     */
    attachInlineEditor(element, format, mainToolbarId, floatedToolbarId) {
      return true;
    },
  };

  // Redirect on hash change when the original hash has an associated CKEditor.
  // @todo

  // Set the CKEditor cache-busting string to the same value as Drupal.
  // @todo
})(
  Drupal,
  drupalSettings,
  ClassicEditor
);
