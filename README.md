CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Uninstall
 * Configuration
 * Maintainers

INTRODUCTION
------------

Replace CKEditor 4 by CKEditor 5.

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

INSTALLATION
------------

Add the following section to the **composer.json** file on your project:

```json
{
    "extra": {
        "patchLevel": {
            "drupal/core": "-p2"
        }
    }
}
```

UNINSTALL
---------

Disable the text formats from the admin page or via drush:

```shell
$ drush ev '$filter = \Drupal::entityTypeManager()->getStorage("filter_format")->load("collaborative_html"); $filter->disable()->save();'
$ drush ev '$filter = \Drupal::entityTypeManager()->getStorage("filter_format")->load("collaborative_full_html"); $filter->disable()->save();'
```

Then you can safely uninstall this extension.


CONFIGURATION
-------------

Customize the gender settings in Administration » Configuration » Open Social
Settings » Real-time collaboration settings.

MAINTAINERS
-----------

Current maintainers:

 * Oleksandr Horbatiuk (chmez) - https://www.drupal.org/user/3071411
 * Serogka Myronets (Agami4) - https://www.drupal.org/user/3061011
 * Volodymyr Sydor (SV) - https://www.drupal.org/user/2642083
 * Roman Salo (rolki) - https://www.drupal.org/user/3595404
 * Taras Kruts (ribel) - https://www.drupal.org/user/1449610
 * Dmytrii Kaiun (tBKoT) - https://www.drupal.org/user/3560652
 * Andriy Chirskyy (Ressinel) - https://www.drupal.org/user/3536075

This project has been sponsored by:

 * Open Social - https://www.drupal.org/open-social
