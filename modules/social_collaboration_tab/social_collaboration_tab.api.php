<?php

/**
 * @file
 * Hooks provided by the Social Collaboration Tab module.
 */

/**
 * Alter the list of collaborative content plugin definitions.
 *
 * @param array $info
 *   The collaborative content plugin definitions to be altered.
 *
 * @see \Drupal\social_collaboration_tab\Annotation\CollaborativeContent
 * @see \Drupal\social_collaboration_tab\CollaborativeContentManager
 */
function hook_collaborative_content_info_alter(array &$info) {
  if (isset($info['event_collaborative_content'])) {
    $info['event_collaborative_content']['bundle'] = 'collaborative_event';
  }
}
