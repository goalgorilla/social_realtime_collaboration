<?php

namespace Drupal\social_collaboration_tab\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form to configure Collaboration Tab settings.
 *
 * @package Drupal\social_collaboration_tab\Form
 */
class SocialCollaborationTabSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['social_collaboration_tab.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'social_collaboration_tab_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('social_collaboration_tab.settings');

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Active'),
      '#default_value' => $config->get('status'),
    ];

    $form['tools'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Tools'),
      '#description' => $this->t('Select CKEditor 5 tools which should be enabled on the node creation/edit page by default.'),
      '#options' => [
        'comment' => $this->t('Comments'),
        'suggestion' => $this->t('Suggestions'),
      ],
      '#states' => [
        'enabled' => [
          ':input[name="status"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    foreach ($form['tools']['#options'] as $key => $title) {
      if ($config->get($key)) {
        $form['tools']['#default_value'][] = $key;
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->config('social_collaboration_tab.settings');

    if ($status = $form_state->getValue('status')) {
      foreach ($form_state->getValue('tools') as $key => $value) {
        $config->set($key, !empty($value));
      }
    }

    $config->set('status', $status)->save();

    parent::submitForm($form, $form_state);
  }

}
