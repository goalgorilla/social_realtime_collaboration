<?php

namespace Drupal\social_collaboration_tab;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Defines an interface for collaborative content plugin.
 *
 * @package Drupal\social_collaboration_tab
 */
interface CollaborativeContentPluginInterface extends ContainerFactoryPluginInterface {

  /**
   * CollaborativeContentBase constructor.
   *
   * @param array $configuration
   *   The given configuration.
   * @param string $plugin_id
   *   The given plugin id.
   * @param mixed $plugin_definition
   *   The given plugin definition.
   * @param int $current_user_id
   *   The current active user ID.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    $current_user_id
  );

  /**
   * Check if the current user is one from node co-authors.
   *
   * @param bool $default_field
   *   TRUE to use a default co-author field.
   *
   * @return bool
   *   TRUE if the current user is a co-author.
   */
  public function isCoAuthor($default_field = TRUE): bool;

  /**
   * Checks access to the collaboration page.
   */
  public function access(): AccessResultInterface;

}
