<?php

namespace Drupal\social_collaboration_tab\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeInterface;
use Drupal\social_collaboration_tab\CollaborativeContentManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Returns responses for Collaboration Tab routes.
 *
 * @package Drupal\social_collaboration_tab\Controller
 */
class SocialCollaborationTabController extends ControllerBase implements SocialCollaborationTabControllerInterface {

  /**
   * The manager.
   */
  protected CollaborativeContentManagerInterface $manager;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    AccountInterface $current_user,
    CollaborativeContentManagerInterface $manager
  ) {
    $this->currentUser = $current_user;
    $this->manager = $manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('current_user'),
      $container->get('plugin.manager.social_collaboration_tab')
    );
  }

  /**
   * The logic to appear/disappear track changes buttons.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node entity object.
   */
  public function trackPermissions(NodeInterface $node): JsonResponse {
    $result = FALSE;

    if ($this->currentUser()->hasPermission('edit any ' . $node->bundle() . ' content')) {
      $result = TRUE;
    }
    else {
      foreach ($this->manager->getDefinitionIdsForNode($node) as $plugin_id) {
        /** @var \Drupal\social_collaboration_tab\CollaborativeContentPluginInterface $plugin */
        $plugin = $this->manager->createInstance($plugin_id);

        if ($plugin->isCoAuthor()) {
          $result = TRUE;

          break;
        }
      }
    }

    return new JsonResponse(['result' => $result]);
  }

}
