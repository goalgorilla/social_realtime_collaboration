<?php

namespace Drupal\social_collaboration_tab\Controller;

use Drupal\Core\Session\AccountInterface;
use Drupal\social_collaboration_tab\CollaborativeContentManagerInterface;

/**
 * Defines an interface for responses of Collaboration Tab routes.
 *
 * @package Drupal\social_collaboration_tab\Controller
 */
interface SocialCollaborationTabControllerInterface {

  /**
   * SocialCollaborationTabController constructor.
   *
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user service.
   * @param \Drupal\social_collaboration_tab\CollaborativeContentManagerInterface $manager
   *   The manager.
   */
  public function __construct(
    AccountInterface $current_user,
    CollaborativeContentManagerInterface $manager
  );

}
