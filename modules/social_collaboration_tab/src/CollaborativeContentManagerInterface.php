<?php

namespace Drupal\social_collaboration_tab;

use Drupal\Core\Plugin\Context\ContextAwarePluginManagerInterface;
use Drupal\node\NodeInterface;

/**
 * Defines an interface for collaborative content manager service.
 *
 * @package Drupal\social_collaboration_tab
 */
interface CollaborativeContentManagerInterface extends ContextAwarePluginManagerInterface {

  /**
   * Determines plugins whose constraints are satisfied by a node.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node entity object.
   */
  public function getDefinitionIdsForNode(NodeInterface $node): array;

}
