<?php

namespace Drupal\social_collaboration_tab;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\Context\ContextAwarePluginManagerTrait;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\node\NodeInterface;
use Drupal\social_collaboration_tab\Annotation\CollaborativeContent;

/**
 * Defines the collaborative content manager service.
 *
 * @package Drupal\social_collaboration_tab
 */
class CollaborativeContentManager extends DefaultPluginManager implements CollaborativeContentManagerInterface {

  use ContextAwarePluginManagerTrait;

  /**
   * The node entity object.
   */
  private NodeInterface $node;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct(
      'Plugin/CollaborativeContent',
      $namespaces,
      $module_handler,
      CollaborativeContentPluginInterface::class,
      CollaborativeContent::class
    );

    $this->alterInfo('collaborative_content_info');
    $this->setCacheBackend($cache_backend, 'collaborative_content_plugins');
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinitionIdsForNode(NodeInterface $node): array {
    $this->node = $node;
    $definitions = $this->getDefinitions();

    foreach ($definitions as $plugin_id => $plugin_definition) {
      if ($plugin_definition['bundle'] !== $node->bundle()) {
        unset($definitions[$plugin_id]);
      }
    }

    return array_keys($definitions);
  }

  /**
   * {@inheritdoc}
   */
  public function createInstance($plugin_id, array $configuration = []): object {
    return parent::createInstance(
      $plugin_id,
      $configuration + ['node' => $this->node]
    );
  }

}
