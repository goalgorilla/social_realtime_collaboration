<?php

namespace Drupal\social_collaboration_tab\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ConfigInstaller;
use Drupal\Core\Config\ConfigManagerInterface;
use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\State\StateInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Extended configuration installer.
 *
 * @package Drupal\social_collaboration_tab\Service
 */
class SocialCollaborationTabConfigInstaller extends ConfigInstaller {

  /**
   * The state storage service.
   */
  protected StateInterface $state;

  /**
   * Constructs extended configuration installer.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\Config\StorageInterface $active_storage
   *   The active configuration storage.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typed_config
   *   The typed configuration manager.
   * @param \Drupal\Core\Config\ConfigManagerInterface $config_manager
   *   The configuration manager.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param string $install_profile
   *   The name of the currently active installation profile.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state storage service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    StorageInterface $active_storage,
    TypedConfigManagerInterface $typed_config,
    ConfigManagerInterface $config_manager,
    EventDispatcherInterface $event_dispatcher,
    $install_profile,
    StateInterface $state
  ) {
    parent::__construct(
      $config_factory,
      $active_storage,
      $typed_config,
      $config_manager,
      $event_dispatcher,
      $install_profile
    );

    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public function checkConfigurationToInstall($type, $name): void {
    if ($this->state->get($key = 'social_collaboration_tab.skip')) {
      $this->state->delete($key);
    }
    else {
      parent::checkConfigurationToInstall($type, $name);
    }
  }

}
