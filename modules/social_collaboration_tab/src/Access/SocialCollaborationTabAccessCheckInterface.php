<?php

namespace Drupal\social_collaboration_tab\Access;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeInterface;

/**
 * Defines an interface for the access checker for the collaboration tab page.
 *
 * @package Drupal\social_collaboration_tab\Access
 */
interface SocialCollaborationTabAccessCheckInterface extends AccessInterface {

  /**
   * Checks access for the collaboration tab page.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account viewing the page.
   * @param \Drupal\node\NodeInterface $node
   *   The node entity object.
   */
  public function access(AccountInterface $account, NodeInterface $node): AccessResultInterface;

}
