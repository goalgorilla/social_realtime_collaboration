<?php

namespace Drupal\social_collaboration_tab\Access;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\entity_access_by_field\EntityAccessHelper;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\Routing\Route;

/**
 * Determines access for group content creation.
 */
class SocialCollaborationGroupContentAccessCheck implements AccessInterface {

  /**
   * The entity type manager.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs a EntityCreateAccessCheck object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Checks access for the collaboration tab page.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account viewing the page.
   * @param \Drupal\node\NodeInterface $node
   *   The node entity object.
   */
  public function access(Route $route, AccountInterface $account, NodeInterface $node): AccessResultInterface {
    $needs_access = $route->getRequirement('_social_collaboration_group_content_access') === 'TRUE';

    // If the route has the requirement to check for group content access,
    // we do care and ask our Entity helper if the account can view the node.
    // If that is the case, we also allow viewing the tab for collaboration.
    $access = EntityAccessHelper::getEntityAccessResult($node, 'view', $account);

    // If the access result based on the visibility is neutral
    // or allowed, we want to deliberately tell Drupal this check is allowing
    // viewing the tab. Based on the SocialCollaborationAccessCheck additional
    // checks are in place to ensure the settings from the Node are taken into
    // account.
    return AccessResult::allowedIf(($access->isNeutral() || $access->isAllowed()) || !$needs_access)->cachePerPermissions()->addCacheableDependency($node);
  }

}
