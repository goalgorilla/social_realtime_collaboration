<?php

namespace Drupal\social_collaboration_tab\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeInterface;
use Drupal\social_collaboration_tab\CollaborativeContentManagerInterface;
use Drupal\social_realtime_collaboration\Service\SocialRealtimeCollaborationHelperInterface;

/**
 * Determines access to for the collaboration tab page.
 *
 * @ingroup social_collaboration_tab_access
 */
class SocialCollaborationTabAccessCheck implements SocialCollaborationTabAccessCheckInterface {

  /**
   * The helper.
   */
  protected SocialRealtimeCollaborationHelperInterface $helper;

  /**
   * The manager.
   */
  protected CollaborativeContentManagerInterface $manager;

  /**
   * Constructs a SocialCollaborationTabAccessCheck object.
   *
   * @param \Drupal\social_realtime_collaboration\Service\SocialRealtimeCollaborationHelperInterface $helper
   *   The helper.
   * @param \Drupal\social_collaboration_tab\CollaborativeContentManagerInterface $manager
   *   The manager.
   */
  public function __construct(
    SocialRealtimeCollaborationHelperInterface $helper,
    CollaborativeContentManagerInterface $manager
  ) {
    $this->helper = $helper;
    $this->manager = $manager;
  }

  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account, NodeInterface $node): AccessResultInterface {
    if (!(
      $account->hasPermission('use real-time collaboration') &&
      $account->hasPermission('view node.' . $node->bundle() . '.field_content_visibility:public content') &&
      $this->helper->isReady() &&
      $this->helper->useCollaboration() &&
      $this->helper->getEditors($account)
    )) {
      return AccessResult::forbidden();
    }

    foreach ($this->manager->getDefinitionIdsForNode($node) as $plugin_id) {
      /** @var \Drupal\social_collaboration_tab\CollaborativeContentPluginInterface $plugin */
      $plugin = $this->manager->createInstance($plugin_id);

      if (!($access = $plugin->access())->isNeutral()) {
        return $access;
      }
    }

    return AccessResult::neutral();
  }

}
