<?php

namespace Drupal\social_collaboration_tab\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Collaborative Content annotation object.
 *
 * @ingroup social_collaboration_tab_api
 *
 * @Annotation
 */
class CollaborativeContent extends Plugin {

  /**
   * The plugin ID.
   */
  public string $id;

  /**
   * The bundle.
   */
  public string $bundle;

}
