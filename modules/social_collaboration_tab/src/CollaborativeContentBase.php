<?php

namespace Drupal\social_collaboration_tab;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\group\Entity\GroupInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a base collaborative content implementation.
 *
 * @ingroup social_collaboration_tab_api
 */
abstract class CollaborativeContentBase extends PluginBase implements CollaborativeContentPluginInterface {

  /**
   * The current active user ID.
   */
  protected int $currentUserId;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    $current_user_id
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->currentUserId = $current_user_id;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user')->id()
    );
  }

  /**
   * {@inheritdoc}
   */
  public function isCoAuthor($default_field = TRUE): bool {
    if ($this->configuration['node']->getOwnerId() === $this->currentUserId) {
      return TRUE;
    }
    elseif (!$default_field) {
      return FALSE;
    }

    /** @var \Drupal\node\NodeInterface $node */
    $node = $this->configuration['node'];

    if (!$node->hasField('field_social_collaboration_editors')) {
      return FALSE;
    }

    /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $field */
    $field = $node->field_social_collaboration_editors;

    foreach ($field->referencedEntities() as $editor) {
      if ($editor->getEntityTypeId() === 'user') {
        if ($editor->id() === $this->currentUserId) {
          return TRUE;
        }
      }
      elseif ($editor instanceof GroupInterface) {
        foreach ($editor->getMembers() as $member) {
          if ($member->getUser()->id() === $this->currentUserId) {
            return TRUE;
          }
        }
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function access(): AccessResultInterface {
    $access = AccessResult::allowedIf($this->isCoAuthor());

    if (!$access->isAllowed()) {
      $node = $this->configuration['node'];

      foreach (['field_comment', 'field_suggestion'] as $field) {
        if ($node->$field->isEmpty() || $node->$field->value) {
          return AccessResult::allowed();
        }
      }

      return AccessResult::forbidden();
    }

    return $access;
  }

}
