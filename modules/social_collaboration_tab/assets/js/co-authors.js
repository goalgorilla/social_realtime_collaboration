(function ($, Drupal) {
  Drupal.behaviors.coAuthors = {
    attach: function (context, settings) {

      let exit = 0;

      const interval = setInterval(() => {
        var toolbar = $('.ck-editor__editable.ck-rounded-corners');

        exit = toolbar.length;

        if (exit > 0) {
          init();
          clearInterval(interval);
        }

      }, 2000);

      function init() {
        var matches = settings.path.currentPath.match(/^\D+\d+/g);

        $.ajax({
          url: Drupal.url(matches[0] + '/track-permissions'),
          type: 'POST',
          dataType: 'json',
          success: function ($data) {
            if ($data.result) {
              showTrackChanges();
            }
            else {
              hideTrackChanges();
            }
          },
          error: function error() {
            console.log('Server error.');
          }
        });
      }

      /**
       * Hide track changes buttons from the CkEditor toolbar.
       */
      function hideTrackChanges() {
        let trackChangesButton = $('button.ck-splitbutton__action');
        let trackChangesArrow = $('button.ck-splitbutton__arrow');

        $('.request-co-authorship').show();
        $('.ck-suggestion-wrapper .ck-annotation button.ck-suggestion--accept').hide();
        $('.ck-suggestion-wrapper .ck-annotation button.ck-suggestion--discard').hide();

        // Enable track changes button only when is it disabled now.
        if (!trackChangesButton.hasClass('ck-on')) {
          trackChangesButton.click();
        }

        trackChangesButton.prop('disabled', true);
        trackChangesButton.hide();

        trackChangesArrow.prop('disabled', true);
        trackChangesArrow.hide();
      }

      /**
       * Show track changes buttons on the CkEditor toolbar
       */
      function showTrackChanges() {
        let trackChangesButton = $('button.ck-splitbutton__action');
        let trackChangesArrow = $('button.ck-splitbutton__arrow');

        $('.request-co-authorship').hide();
        $('.ck-suggestion-wrapper .ck-annotation button.ck-suggestion--accept').show();
        $('.ck-suggestion-wrapper .ck-annotation button.ck-suggestion--discard').show();

        trackChangesButton.prop('disabled', false);
        trackChangesButton.show();

        trackChangesArrow.prop('disabled', false);
        trackChangesArrow.show();
      }
    }
  };
})(jQuery, Drupal);
