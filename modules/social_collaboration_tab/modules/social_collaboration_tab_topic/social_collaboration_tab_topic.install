<?php

/**
 * @file
 * Install, update and uninstall functions for the topic tab module.
 */

use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Implements hook_install().
 */
function social_collaboration_tab_topic_install(): void {
  _social_collaboration_tab_topic_form();
}

/**
 * Set the position of fields for managing comments and suggestions.
 */
function _social_collaboration_tab_topic_form(): void {
  /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $repository */
  $repository = \Drupal::service('entity_display.repository');

  $form_display = $repository->getFormDisplay('node', 'topic');

  $group = 'group_settings';
  $settings = $form_display->getThirdPartySetting('field_group', $group);
  $name = 'field_topic_comments';

  if (!$settings) {
    $group = 'group_topic_description';
    $settings = $form_display->getThirdPartySetting('field_group', $group);
    $name = 'body';
  }

  $field = $form_display->getComponent($name);
  $weight = $field ? $field['weight'] + 1 : 0;
  $updated = FALSE;

  foreach (['field_suggestion', 'field_comment'] as $name) {
    if (!in_array($name, $settings['children'])) {
      $form_display->setComponent($name, [
        'weight' => $weight,
        'settings' => [],
        'third_party_settings' => [],
        'type' => 'boolean_checkbox',
        'region' => 'content',
      ]);

      $settings['children'][] = $name;
      $updated = TRUE;
    }
  }

  if ($updated) {
    $form_display->setThirdPartySetting('field_group', $group, $settings);
    $form_display->save();
  }
}

/**
 * Implements hook_update_dependencies().
 */
function social_collaboration_tab_topic_update_dependencies(): array {
  return [
    'social_collaboration_tab_topic' => [
      8001 => [
        'social_collaboration_tab' => 8802,
      ],
    ],
  ];
}

/**
 * Add fields for managing access to comments and suggestions.
 */
function social_collaboration_tab_topic_update_8001(): void {
  $prefix = \Drupal::service('extension.list.module')->getPath('social_collaboration_tab_topic') .
    '/config/install/field.field.node.topic.';

  /** @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface $storage */
  $storage = \Drupal::entityTypeManager()->getStorage('field_config');

  foreach (['field_comment', 'field_suggestion'] as $field) {
    $settings = Yaml::parseFile($prefix . $field . '.yml');
    $storage->createFromStorageRecord($settings)->save();
  }
}

/**
 * Set the position of fields for managing comments and suggestions.
 */
function social_collaboration_tab_topic_update_8002(): void {
  _social_collaboration_tab_topic_form();
}

/**
 * Delete default value of fields.
 */
function social_collaboration_tab_topic_update_8003(): void {
  /** @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface $storage */
  $storage = \Drupal::entityTypeManager()->getStorage('field_config');

  foreach (['field_comment', 'field_suggestion'] as $field) {
    /** @var \Drupal\field\FieldConfigInterface $field_config */
    $field_config = $storage->load('node.topic.' . $field);

    $field_config->set('default_value', [])->save();
  }
}

/**
 * Check if the topic edit page is not broken.
 */
function social_collaboration_tab_topic_update_8004(): string {
  $output = [];

  /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $repository */
  $repository = \Drupal::service('entity_display.repository');

  $display = $repository->getFormDisplay('node', 'topic', 'collaboration');

  if ($display->isNew()) {
    $repository
      ->getFormDisplay('node', 'topic')
      ->createCopy('collaboration')
      ->save();

    $output[] = t('Configuration for the topic collaboration form has been created.');
  }

  $config_name = 'core.entity_form_display.node.topic.' . EntityDisplayRepositoryInterface::DEFAULT_DISPLAY_MODE;
  $config = \Drupal::configFactory()->getEditable($config_name);

  if ($config->get('hidden.title') !== NULL) {
    $file_name = sprintf(
      '%s/config/install/%s.yml',
      \Drupal::service('extension.list.module')->getPath('social_topic'),
      $config_name
    );

    $config->setData(Yaml::parseFile($file_name))->save();

    $output[] = t('The topic edit page has been restored.');
  }

  $output = implode(PHP_EOL, array_reverse($output));

  if (is_callable($function = 'drush_print')) {
    $function($output);
  }

  return $output;
}

/**
 * Restore fields for managing comments and suggestions.
 */
function social_collaboration_tab_topic_update_8005(): void {
  _social_collaboration_tab_topic_form();
}
