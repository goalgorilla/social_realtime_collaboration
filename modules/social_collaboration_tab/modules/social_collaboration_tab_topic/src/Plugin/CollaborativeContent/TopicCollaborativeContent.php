<?php

namespace Drupal\social_collaboration_tab_topic\Plugin\CollaborativeContent;

use Drupal\social_collaboration_tab\CollaborativeContentBase;

/**
 * Provides a collaborative content for topics.
 *
 * @CollaborativeContent(
 *   id = "topic_collaborative_content",
 *   bundle = "topic",
 * )
 */
class TopicCollaborativeContent extends CollaborativeContentBase {

}
