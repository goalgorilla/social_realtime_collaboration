<?php

namespace Drupal\social_collaboration_tab_idea\Plugin\QueueWorker;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\activity_creator\Plugin\ActivityActionManager;
use Drupal\node\NodeInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Suggestion worker.
 *
 * @QueueWorker(
 *   id = "social_collaboration_tab_idea.suggestion",
 *   title = @Translation("Suggestions worker"),
 *   cron = {"time" = 30}
 * )
 */
class SuggestionsQueue extends QueueWorkerBase implements SuggestionsQueueInterface {

  /**
   * The entity type manager.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The time service.
   */
  protected TimeInterface $time;

  /**
   * The activity action manager.
   */
  protected ActivityActionManager $activityActionManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    TimeInterface $time,
    ActivityActionManager $activity_action_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->time = $time;
    $this->activityActionManager = $activity_action_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('datetime.time'),
      $container->get('plugin.manager.activity_action.processor')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {
    // Get node from suggestion.
    $entity = $this->entityTypeManager->getStorage('node')->load($data['nid']);

    if ($entity instanceof NodeInterface) {
      $entity->setCreatedTime($this->time->getRequestTime());

      // Get user from suggestion.
      $user = $this->entityTypeManager->getStorage('user')->load($data['uid']);

      if ($user instanceof UserInterface) {
        $entity->setOwner($user);

        // Create new activity.
        /** @var \Drupal\activity_creator\Plugin\ActivityActionInterface $plugin */
        $plugin = $this->activityActionManager->createInstance('suggest_entity_action');

        $plugin->create($entity);
      }
    }
  }

}
