<?php

namespace Drupal\social_collaboration_tab_idea\Plugin\QueueWorker;

use Drupal\activity_creator\Plugin\ActivityActionManager;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Defines an interface for suggestion worker.
 *
 * @package Drupal\social_collaboration_tab_idea\Plugin\QueueWorker
 */
interface SuggestionsQueueInterface extends ContainerFactoryPluginInterface {

  /**
   * SuggestionsQueue constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity manager service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\activity_creator\Plugin\ActivityActionManager $activity_action_manager
   *   The activity action manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    TimeInterface $time,
    ActivityActionManager $activity_action_manager
  );

}
