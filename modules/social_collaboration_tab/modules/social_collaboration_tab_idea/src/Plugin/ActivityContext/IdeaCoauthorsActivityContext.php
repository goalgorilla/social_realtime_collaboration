<?php

namespace Drupal\social_collaboration_tab_idea\Plugin\ActivityContext;

use Drupal\activity_creator\Plugin\ActivityContextBase;
use Drupal\node\NodeInterface;

/**
 * Provides a 'IdeaCoauthorsActivityContext' activity context.
 *
 * @ActivityContext(
 *   id = "idea_coauthors_activity_context",
 *   label = @Translation("Idea coauthors activity context"),
 * )
 */
class IdeaCoauthorsActivityContext extends ActivityContextBase {

  /**
   * {@inheritdoc}
   */
  public function getRecipients(array $data, $last_uid, $limit): array {
    $recipients = [];

    // We only know the context if there is a related object.
    if (isset($data['related_object']) && !empty($data['related_object'])) {
      if ($data['related_object'][0]['target_type'] === 'node') {
        $recipients = $this->getRecipientsCoauthors($data);
      }
    }

    return $recipients;
  }

  /**
   * Returns idea coauthors.
   *
   * @param array $data
   *   The data.
   *
   * @return array
   *   An associative array of recipients, containing the following key-value
   *   pairs:
   *   - target_type: The entity type ID.
   *   - target_id: The entity ID.
   */
  public function getRecipientsCoauthors(array $data): array {
    $recipients = [];

    foreach ($this->getCoauthors($data) as $co_author) {
      $recipients[] = [
        'target_type' => 'user',
        'target_id' => $co_author,
      ];
    }

    return $recipients;
  }

  /**
   * Returns idea coauthors.
   *
   * @param array $data
   *   The data.
   *
   * @return array
   *   An array of coauthors ids.
   */
  public function getCoauthors(array $data): array {
    $related_object = $data['related_object'][0];
    $storage = $this->entityTypeManager->getStorage($related_object['target_type']);
    $node = $storage->load($related_object['target_id']);
    $coauthors = [];

    if ($node instanceof NodeInterface) {
      $owner_id = $node->getOwnerId();
      $coauthors[] = $owner_id;

      if (
        $node->hasField('field_social_idea_coauthors') &&
        !$node->field_social_idea_coauthors->isEmpty()
      ) {
        $coauthors_field = $node->field_social_idea_coauthors->getValue();

        foreach ($coauthors_field as $co_author) {
          if ($co_author['target_id'] !== $owner_id) {
            $coauthors[] = $co_author['target_id'];
          }
        }
      }
    }

    return $coauthors;
  }

}
