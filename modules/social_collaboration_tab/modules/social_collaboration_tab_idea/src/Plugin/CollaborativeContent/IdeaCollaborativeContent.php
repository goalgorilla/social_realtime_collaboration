<?php

namespace Drupal\social_collaboration_tab_idea\Plugin\CollaborativeContent;

use Drupal\social_collaboration_tab\CollaborativeContentBase;

/**
 * Provides a collaborative content for ideas.
 *
 * @CollaborativeContent(
 *   id = "idea_collaborative_content",
 *   bundle = "social_idea",
 * )
 */
class IdeaCollaborativeContent extends CollaborativeContentBase {

  /**
   * {@inheritdoc}
   */
  public function isCoAuthor($default_field = TRUE): bool {
    return parent::isCoAuthor(FALSE) || in_array(
      $this->currentUserId,
      array_column($this->configuration['node']->field_social_idea_coauthors->getValue(), 'target_id')
    );
  }

}
