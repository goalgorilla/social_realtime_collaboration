<?php

namespace Drupal\social_collaboration_tab_idea\Plugin\ActivityAction;

use Drupal\activity_creator\Plugin\ActivityActionBase;

/**
 * Provides a 'SuggestActivityAction' activity action.
 *
 * @ActivityAction(
 *   id = "suggest_entity_action",
 *   label = @Translation("Action that is triggered when new suggestion added"),
 * )
 */
class SuggestActivityAction extends ActivityActionBase {}
