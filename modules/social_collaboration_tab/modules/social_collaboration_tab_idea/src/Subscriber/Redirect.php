<?php

namespace Drupal\social_collaboration_tab_idea\Subscriber;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\group\Entity\GroupInterface;
use Drupal\node\NodeInterface;
use Drupal\social_challenge\ChallengeWrapper;
use Drupal\social_challenge_phase\Entity\Phase;
use Drupal\social_challenge_phase\Service\SocialChallengePhasePermissions;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Redirect to the collaboration tab.
 *
 * @package Drupal\social_collaboration_tab_idea\Subscriber
 */
class Redirect implements EventSubscriberInterface {

  /**
   * The current active route match object.
   */
  protected RouteMatchInterface $routeMatch;

  /**
   * The current user.
   */
  protected AccountProxyInterface $currentUser;

  /**
   * The challenge phase permissions service.
   */
  protected SocialChallengePhasePermissions $challengePhasePermissions;

  /**
   * The challenge wrapper.
   */
  protected ChallengeWrapper $challengeWrapper;

  /**
   * Redirect constructor.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current active route match object.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Drupal\social_challenge_phase\Service\SocialChallengePhasePermissions $challenge_phase_permissions
   *   The social challenge phase permissions service.
   * @param \Drupal\social_challenge\ChallengeWrapper $challenge_wrapper
   *   The challenge wrapper.
   */
  public function __construct(
    RouteMatchInterface $route_match,
    AccountProxyInterface $current_user,
    SocialChallengePhasePermissions $challenge_phase_permissions,
    ChallengeWrapper $challenge_wrapper
  ) {
    $this->routeMatch = $route_match;
    $this->currentUser = $current_user;
    $this->challengePhasePermissions = $challenge_phase_permissions;
    $this->challengeWrapper = $challenge_wrapper;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[KernelEvents::REQUEST][] = ['checkForRedirection'];
    return $events;
  }

  /**
   * This method is called when the KernelEvents::REQUEST event is dispatched.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The event.
   */
  public function checkForRedirection(RequestEvent $event): void {
    if (
      $this->routeMatch->getRouteName() !== 'entity.node.canonical' ||
      !$this->currentUser->hasPermission('use real-time collaboration')
    ) {
      return;
    }

    $node = $this->routeMatch->getParameter('node');

    if (!$node instanceof NodeInterface || $node->bundle() !== 'social_idea') {
      return;
    }

    // As LU when clicking on an idea I will land on the description tab,
    // except for authors and co-authors who will go directly the collaboration
    // tab.
    $coauthors_ids = array_column($node->field_social_idea_coauthors->getValue(), 'target_id');
    // Make array with author and co-authors.
    $coauthors_ids[] = $node->getOwnerId();
    // Check if current user is author whether co-author.
    if (!in_array($this->currentUser->id(), $coauthors_ids)) {
      return;
    }

    $group = _social_group_get_current_group();
    if (!$group instanceof GroupInterface) {
      return;
    }

    $this->challengeWrapper->setChallenge($group);

    $active_phase = $this->challengeWrapper->getActivePhase();
    if (!$active_phase instanceof Phase) {
      return;
    }

    $http_referer = $event->getRequest()->headers->get('referer', '');

    if (
      (
        !$this->challengePhasePermissions->hasPermissionValue('create idea in phase', $active_phase) &&
        !$this->challengePhasePermissions->hasPermissionValue('comment in phase', $active_phase)
      ) ||
      (
        is_string($http_referer) &&
        (
          strpos($http_referer, 'collaboration') ||
          strpos($http_referer, $event->getRequest()->getRequestUri())
        )
      )
    ) {
      return;
    }

    $url = Url::fromRoute('entity.node.collaboration', ['node' => $node->id()])
      ->toString();

    $response = new RedirectResponse($url);
    $event->setResponse($response);
  }

}
