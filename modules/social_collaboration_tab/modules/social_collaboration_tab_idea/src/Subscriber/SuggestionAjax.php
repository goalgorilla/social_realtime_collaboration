<?php

namespace Drupal\social_collaboration_tab_idea\Subscriber;

use Drupal\Core\TempStore\SharedTempStoreFactory;
use Drupal\social_realtime_collaboration\Event\SocialRealtimeCollaborationSuggestionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event subscriber to suggestion ajax.
 */
class SuggestionAjax implements EventSubscriberInterface {

  /**
   * The shared tempstore factory.
   */
  protected SharedTempStoreFactory $sharedTempStore;

  /**
   * SuggestionAjax constructor.
   *
   * @param \Drupal\Core\TempStore\SharedTempStoreFactory $shared_temp_store
   *   The shared tempstore factory.
   */
  public function __construct(SharedTempStoreFactory $shared_temp_store) {
    $this->sharedTempStore = $shared_temp_store;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[SocialRealtimeCollaborationSuggestionEvent::SOCIAL_REALTIME_COLLABORATION_AJAX_EVENT][] = ['suggestionAjax'];
    return $events;
  }

  /**
   * Handles on suggestion ajax event.
   *
   * @param \Drupal\social_realtime_collaboration\Event\SocialRealtimeCollaborationSuggestionEvent $event
   *   The suggestion event.
   */
  public function suggestionAjax(SocialRealtimeCollaborationSuggestionEvent $event): void {
    // Get parameters from suggestion event.
    $params = $event->getParams();

    // Get all existing suggestions in shared temporary storage.
    $suggestions_collection = $this->sharedTempStore->get('collaboration_tab_suggestions');
    $suggestions = $suggestions_collection->get('suggestions');

    // Delete suggestion if it need to be deleted else create if it not exist.
    if ($params['delete']) {
      unset($suggestions[$params['sid']]);
    }
    elseif (!isset($suggestions[$params['sid']])) {
      $suggestions[$params['sid']] = [
        'nid' => $params['nid'],
        'uid' => $params['uid'],
      ];
    }

    // Set new suggestions to shared temporary storage.
    $suggestions_collection->set('suggestions', $suggestions);
  }

}
