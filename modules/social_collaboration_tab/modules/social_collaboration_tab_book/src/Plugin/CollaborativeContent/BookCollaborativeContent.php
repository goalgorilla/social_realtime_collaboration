<?php

namespace Drupal\social_collaboration_tab_book\Plugin\CollaborativeContent;

use Drupal\social_collaboration_tab\CollaborativeContentBase;

/**
 * Provides a collaborative content for books.
 *
 * @CollaborativeContent(
 *   id = "book_collaborative_content",
 *   bundle = "book",
 * )
 */
class BookCollaborativeContent extends CollaborativeContentBase {

}
