(function ($) {
  Drupal.behaviors.quickeditCkeditor = {
    attach: function (context, settings) {
      $(window).on('load', function () {
        var $hideContent = $('.region--complementary, #section-comments + section, .site-footer');

        setTimeout(function () {
          $('.quickedit > a').once('quickedit-open').on('click', function () {

            // Wait on load quickedit attributes of editable elements.
            setTimeout(function () {

              // Display editable body field and hide other elements.
              $('.body-text').once('quickedit-start').on('click', function () {
                $hideContent.each(function () {
                  $(this).addClass('open-quickedit');
                });
              });
            }, 500);
          });
        }, 500);

        // Display hidden elements after data has been saved/canceled.
        $( document ).ajaxComplete(function() {
          $('.action-save.quickedit-button').once('quickedit-clear').on('click', function () {
            revertSidebar();
          });
          $('.action-cancel.quickedit-button').once('quickedit-cancel').on('click', function () {
            revertSidebar();
          });
        });

        function revertSidebar() {
          if ($('.quickedit-form').length === 1) {
            setTimeout(function () {
              $hideContent.each(function () {
                $(this).removeClass('open-quickedit');
              });
            }, 500);
          }
        }

      });
    }
  };
})(jQuery);
