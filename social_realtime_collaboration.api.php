<?php

/**
 * @file
 * Hooks provided by the Social Real-time Collaboration module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Returns form IDs which text field formats should be filtered.
 *
 * @return array
 *   The form IDs.
 *
 * @see \Drupal\social_collaboration_tab\Service\SocialRealtimeCollaborationHelper::processFormat()
 */
function hook_social_realtime_collaboration_forms(): array {
  return ['node_book_edit_form', 'node_event_edit_form'];
}

/**
 * @} End of "addtogroup hooks".
 */
