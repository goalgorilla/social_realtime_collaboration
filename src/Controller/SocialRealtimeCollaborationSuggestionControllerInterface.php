<?php

namespace Drupal\social_realtime_collaboration\Controller;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Defines an interface for controller for suggestion events.
 *
 * @package Drupal\social_realtime_collaboration\Controller
 */
interface SocialRealtimeCollaborationSuggestionControllerInterface {

  /**
   * SocialRealtimeCollaborationSuggestionController constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The new request stack.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher service.
   */
  public function __construct(
    RequestStack $request_stack,
    EventDispatcherInterface $event_dispatcher
  );

}
