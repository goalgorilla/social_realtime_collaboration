<?php

namespace Drupal\social_realtime_collaboration\Controller;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\profile\Entity\ProfileInterface;
use Drupal\social_realtime_collaboration\Service\SocialRealtimeCollaborationHelperInterface;
use Firebase\JWT\JWT;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Returns responses for Real-time Collaboration routes.
 *
 * @package Drupal\social_realtime_collaboration\Controller
 */
class SocialRealtimeCollaborationController extends ControllerBase implements SocialRealtimeCollaborationControllerInterface {

  /**
   * The time service.
   */
  protected TimeInterface $time;

  /**
   * The helper.
   */
  protected SocialRealtimeCollaborationHelperInterface $helper;

  /**
   * The URL generator.
   */
  protected FileUrlGeneratorInterface $fileUrlGenerator;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    AccountInterface $current_user,
    TimeInterface $time,
    SocialRealtimeCollaborationHelperInterface $helper
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $current_user;
    $this->time = $time;
    $this->helper = $helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    $instance = new static(
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('datetime.time'),
      $container->get('social_realtime_collaboration.helper'),
    );

    // Needed because of backwards compatibility, remove once
    // we drop support for Drupal < 9.3 
    if ($container->has('file_url_generator')) {
      $instance->setFileUrlGenerator($container->get('file_url_generator'));
    }

    return $instance;
  }

  /**
   * Sets the file url generator service, introduced in 9.3.
   *
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator
   *   The file url generator.
   */
  public function setFileUrlGenerator(FileUrlGeneratorInterface $file_url_generator) {
    $this->fileUrlGenerator = $file_url_generator;
  }

  /**
   * Create JSON web token.
   *
   * @see https://ckeditor.com/docs/cs/latest/examples/token-endpoints/php.html
   */
  public function token(): Response {
    /** @var \Drupal\user\UserInterface $account */
    $account = $this->entityTypeManager()->getStorage('user')
      ->load($this->currentUser()->id());

    $payload_user = [
      'id' => 'user-' . $account->id(),
      'email' => $account->getEmail(),
      'name' => $account->getDisplayName(),
    ];

    /** @var \Drupal\profile\ProfileStorageInterface $storage */
    $storage = $this->entityTypeManager()->getStorage('profile');

    $profile = $storage->loadByUser($account, 'profile');

    if (
      $profile instanceof ProfileInterface &&
      !$profile->field_profile_image->isEmpty()
    ) {
      /** @var \Drupal\image\ImageStyleInterface $image_style */
      $image_style = $this->entityTypeManager()->getStorage('image_style')
        ->load('social_medium');
      /** @var \Drupal\file\FileInterface $image */
      $image = $profile->field_profile_image->entity;

      $image_uri = $image->getFileUri();
      $url = $image_style->buildUrl($image_uri);

      if ($this->fileUrlGenerator) {
        $payload_user['avatar'] = $this->fileUrlGenerator->transformRelative($url);
      }
      else {
        // @phpstan-ignore-next-line
        $payload_user['avatar'] = file_url_transform_relative($url);
      }
    }

    $payload = [
      'aud' => $this->helper->get('environment'),
      'iat' => $this->time->getRequestTime(),
      'sub' => $payload_user['id'],
      'user' => $payload_user,
      'auth' => [
        'collaboration' => [
          '*' => [
            'role' => 'writer',
          ],
        ],
      ],
    ];

    $content = JWT::encode($payload, $this->helper->get('secret'));

    return new Response($content, Response::HTTP_OK, [
      'Content-Type' => 'text/plain',
    ]);
  }

}
