<?php

namespace Drupal\social_realtime_collaboration\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\social_realtime_collaboration\Event\SocialRealtimeCollaborationSuggestionEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller for suggestion events.
 */
class SocialRealtimeCollaborationSuggestionController extends ControllerBase implements SocialRealtimeCollaborationSuggestionControllerInterface {

  /**
   * The new request stack.
   */
  protected RequestStack $requestStack;

  /**
   * The event dispatcher service.
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    RequestStack $request_stack,
    EventDispatcherInterface $event_dispatcher
  ) {
    $this->requestStack = $request_stack;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('request_stack'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * Trigger event on ajax when suggestion added/deleted.
   */
  public function triggerEvent(): Response {
    $current_request = $this->requestStack->getCurrentRequest();

    if ($current_request !== NULL) {
      $event = new SocialRealtimeCollaborationSuggestionEvent($current_request->request->all());
      // Ignore the error about arguments quantity because the dispatch method
      // takes arguments using func_get_arg().
      // @phpstan-ignore-next-line
      $this->eventDispatcher->dispatch($event, SocialRealtimeCollaborationSuggestionEvent::SOCIAL_REALTIME_COLLABORATION_AJAX_EVENT);
    }

    return new Response();
  }

}
