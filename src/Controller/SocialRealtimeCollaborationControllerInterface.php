<?php

namespace Drupal\social_realtime_collaboration\Controller;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\social_realtime_collaboration\Service\SocialRealtimeCollaborationHelperInterface;

/**
 * Defines an interface for responses of Real-time Collaboration routes.
 *
 * @package Drupal\social_realtime_collaboration\Controller
 */
interface SocialRealtimeCollaborationControllerInterface {

  /**
   * SocialRealtimeCollaborationController constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\social_realtime_collaboration\Service\SocialRealtimeCollaborationHelperInterface $helper
   *   The helper.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    AccountInterface $current_user,
    TimeInterface $time,
    SocialRealtimeCollaborationHelperInterface $helper
  );

}
