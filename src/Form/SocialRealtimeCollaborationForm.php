<?php

namespace Drupal\social_realtime_collaboration\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\social_realtime_collaboration\Service\SocialRealtimeCollaborationHelperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * Provides a form to configure Real-time Collaboration settings.
 */
class SocialRealtimeCollaborationForm extends ConfigFormBase implements SocialRealtimeCollaborationFormInterface, TrustedCallbackInterface {

  /**
   * The state storage service.
   */
  protected StateInterface $state;

  /**
   * The helper.
   */
  protected SocialRealtimeCollaborationHelperInterface $helper;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    TranslationInterface $string_translation,
    StateInterface $state,
    MessengerInterface $messenger,
    SocialRealtimeCollaborationHelperInterface $helper
  ) {
    parent::__construct($config_factory);

    $this->setStringTranslation($string_translation);
    $this->state = $state;
    $this->setMessenger($messenger);
    $this->helper = $helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('config.factory'),
      $container->get('string_translation'),
      $container->get('state'),
      $container->get('messenger'),
      $container->get('social_realtime_collaboration.helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['social_realtime_collaboration.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'social_realtime_collaboration_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['keys'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('API keys'),
    ];

    $form['keys']['type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Type'),
      '#options' => [
        'production' => $this->t('Production'),
        'development' => $this->t('Development'),
      ],
      '#default_value' => $this->helper->get('type', 'production'),
      '#pre_render' => [[self::class, 'preRenderType']],
    ];

    $form['keys']['production'] = [
      '#type' => 'container',
      '#states' => [
        'visible' => [
          ':input[name="type"]' => [
            'value' => 'production',
          ],
        ],
      ],
    ];

    $form['keys']['production']['environment'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Environment'),
      '#description' => $this->t('Copy value from the <em>Environment ID</em> row on the <em>API configuration</em> tab.'),
      '#maxlength' => 20,
      '#size' => 30,
      '#default_value' => $this->helper->get('environment'),
      '#states' => [
        'required' => [
          ':input[name="type"]' => [
            'value' => 'production',
          ],
        ],
      ],
    ];

    $form['keys']['production']['secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret'),
      '#description' => $this->t('Press the <em>Show</em> button and copy the value for the needed key on the <em>Access credentials</em> tab.'),
      '#maxlength' => 60,
      '#size' => 70,
      '#default_value' => $this->helper->get('secret'),
      '#states' => [
        'required' => [
          ':input[name="type"]' => [
            'value' => 'production',
          ],
        ],
      ],
    ];

    $form['keys']['development'] = [
      '#type' => 'container',
      '#states' => [
        'visible' => [
          ':input[name="type"]' => [
            'value' => 'development',
          ],
        ],
      ],
    ];

    $form['keys']['development']['url'] = [
      '#type' => 'url',
      '#title' => $this->t('URL'),
      '#description' => $this->t('Copy value from the %field field.', [
        '%field' => 'Development token URL',
      ]),
      '#default_value' => $this->helper->get('url'),
      '#attributes' => [
        'placeholder' => $this->t('E.g.: @value', [
          '@value' => 'https://12345.cke-cs.com/token/dev/SXxCBlkpAQECpOhlbgPbbXssebbGUob41ThKkYNny11XA3RwFNjTEqi0vTqO',
        ]),
      ],
      '#states' => [
        'required' => [
          ':input[name="type"]' => [
            'value' => 'development',
          ],
        ],
      ],
    ];

    $form['keys']['development']['description'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'class' => ['description'],
      ],
      '#value' => $this->t('Press the %button button for open the %window window.', [
        '%button' => 'CKEditor configuration',
        '%window' => 'Collaboration configuration',
      ]),
    ];

    $form['urls'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Collaboration URLs'),
      '#description' => $this->t('Navigate to the <em>CKEditor configuration</em> tab.'),
    ];

    $form['urls']['upload'] = [
      '#type' => 'url',
      '#title' => $this->t('Upload'),
      '#description' => $this->t('Copy value from the %row row.', [
        '%row' => 'Easy Image upload URL',
      ]),
      '#attributes' => [
        'placeholder' => $this->t('E.g.: @value', [
          '@value' => 'https://12345.cke-cs.com/easyimage/upload/',
        ]),
      ],
      '#default_value' => $this->helper->get('upload'),
      '#required' => TRUE,
    ];

    $form['urls']['websocket'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Websocket'),
      '#description' => $this->t('Copy value from the %row row.', [
        '%row' => 'Websocket URL',
      ]),
      '#maxlength' => 25,
      '#size' => 35,
      '#attributes' => [
        'placeholder' => $this->t('E.g.: @value', [
          '@value' => '12345.cke-cs.com/ws',
        ]),
      ],
      '#default_value' => $this->helper->get('websocket'),
      '#required' => TRUE,
    ];

    $config = $this->config($this->getEditableConfigNames()[0]);

    $form['override'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use CKEditor 5 for the "Full HTML" text format'),
      '#default_value' => $config->get('override'),
    ];

    $form['entities'] = [
      '#type' => 'fieldset',
    ];

    $form['entities']['markup'] = [
      '#markup' => '<strong>' . $this->t('Allowed content types') . '</strong><br><small>' . $this->t('Please choose for which types of content can be used RTC') . '</small>',
    ];

    $form['entities']['allowed_content_types'] = [
      '#type' => 'checkboxes',
      '#default_value' => $config->get('allowed_content_types') ?: [],
      '#options' => $this->getContentTypes(),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Get allowed content types for RTC.
   */
  public function getContentTypes(): array {
    $content_types = node_type_get_names();
    $allowed_types = $this->getAllowedContentTypes();

    $types = [];
    foreach ($content_types as $key => $type) {
      if (in_array($key, $allowed_types)) {
        $types[$key] = $type;
      }
    }

    return $types;
  }

  /**
   * Get the allowed node bundles for using RTC.
   *
   * @return string[]
   *   List of allowed node bundles.
   */
  private function getAllowedContentTypes(): array {
    return [
      'book',
      'topic',
      'social_idea',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    parent::validateForm($form, $form_state);

    $fields = ['upload', 'websocket'];
    $type = $form_state->getValue('type');

    if ($is_production = $type === 'production') {
      $fields = array_merge($fields, ['environment', 'secret']);
    }
    else {
      $fields[] = 'url';
    }

    $subdomains = [];

    $patterns = [
      'url' => '/^https:\/\/(\d{5})\.cke-cs\.com\/token\/dev\/[a-zA-Z0-9]+$/',
      'upload' => '/^https:\/\/(\d{5})\.cke-cs\.com\/easyimage\/upload\/$/',
      'websocket' => '/^(\d{5})\.cke-cs\.com\/ws$/',
    ];

    foreach ($fields as $field) {
      if (!($value = $form_state->getValue($field))) {
        continue;
      }

      $valid = TRUE;

      if (isset($patterns[$field])) {
        if (preg_match($patterns[$field], $value, $matches)) {
          $subdomains[] = $matches[1];
        }
        else {
          $valid = FALSE;
        }
      }
      elseif (strlen($value) !== $form['keys'][$type][$field]['#maxlength']) {
        $valid = FALSE;
      }

      if (!$valid) {
        $form_state->setErrorByName($field, $this->t('This value is not valid.'));
      }
    }

    if (
      count($subdomains) === 2 + (int) $is_production &&
      count(array_unique($subdomains)) > 1
    ) {
      $form_state->setErrorByName('urls', $this->t('Subdomains are not the same.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $type_fields = [
      TRUE => ['environment', 'secret'],
      FALSE => ['url'],
    ];

    $is_production = $form_state->getValue('type') === 'production';

    $fields = array_merge(
      ['type', 'upload', 'websocket'],
      $type_fields[$is_production]
    );

    foreach ($fields as $field) {
      $this->state->set('social_realtime_collaboration.' . $field, $form_state->getValue($field));
    }

    $this->state->deleteMultiple(array_map(function ($field) {
      return 'social_realtime_collaboration.' . $field;
    }, $type_fields[!$is_production]));

    $config = $this->config($this->getEditableConfigNames()[0]);
    $config->set('override', $form_state->getValue('override'));
    $config->set('allowed_content_types', $form_state->getValue('allowed_content_types'));
    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Add a description to each option.
   *
   * @param array $element
   *   An associative array containing the properties and children of the
   *   element.
   */
  public static function preRenderType(array $element): array {
    $element['production']['#description'] = t('Generate token per user based on environment ID and secret key.');
    $element['development']['#description'] = t('Use a pre-generated token by the CKEditor Ecosystem platform.');
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks(): array {
    return ['preRenderType'];
  }

}
