<?php

namespace Drupal\social_realtime_collaboration\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\social_realtime_collaboration\Service\SocialRealtimeCollaborationHelperInterface;

/**
 * Defines an interface for a form to configure module settings.
 *
 * @package Drupal\social_realtime_collaboration\Form
 */
interface SocialRealtimeCollaborationFormInterface {

  /**
   * SocialRealtimeCollaborationForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state storage service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\social_realtime_collaboration\Service\SocialRealtimeCollaborationHelperInterface $helper
   *   The helper.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    TranslationInterface $string_translation,
    StateInterface $state,
    MessengerInterface $messenger,
    SocialRealtimeCollaborationHelperInterface $helper
  );

}
