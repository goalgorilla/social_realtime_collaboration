<?php

namespace Drupal\social_realtime_collaboration\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Define event for suggestion ajax controller.
 */
class SocialRealtimeCollaborationSuggestionEvent extends Event {

  const SOCIAL_REALTIME_COLLABORATION_AJAX_EVENT = 'social_realtime_collaboration.ajax_event';

  /**
   * Array of POST parameters.
   */
  protected array $params;

  /**
   * SocialRealtimeCollaborationSuggestionEvent constructor.
   *
   * @param array $params
   *   The POST parameters.
   */
  public function __construct(array $params) {
    $this->params = $params;
  }

  /**
   * Returns parameters from event.
   */
  public function getParams(): array {
    return $this->params;
  }

}
