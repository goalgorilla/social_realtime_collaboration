<?php

namespace Drupal\social_realtime_collaboration\Plugin\Filter;

use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Drupal\social_realtime_collaboration\Service\SocialRealtimeCollaborationHelperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a filter to suggestion elements.
 *
 * @Filter(
 *   id = "social_realtime_collaboration_suggestion",
 *   title = @Translation("Suggestion"),
 *   description = @Translation("Delete content between the <code>&lt;suggestion&gt;</code> paired tags."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE,
 *   weight = 12
 * )
 */
class SocialRealtimeCollaborationSuggestion extends FilterBase implements SocialRealtimeCollaborationSuggestionInterface {

  /**
   * The helper.
   */
  protected SocialRealtimeCollaborationHelperInterface $helper;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    SocialRealtimeCollaborationHelperInterface $helper
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->helper = $helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('social_realtime_collaboration.helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode): FilterProcessResult {
    $result = new FilterProcessResult($text);

    if (
      $this->helper->isReady() &&
      preg_match_all('/<suggestion id="([^"]+)" suggestion-type="insertion" type="start">/', $text, $matches)
    ) {
      foreach ($matches[1] as $delta => $id) {
        $start = mb_strpos($text, $matches[0][$delta]);

        if ($start === FALSE) {
          continue;
        }

        $tag = '<suggestion id="' . $id . '" suggestion-type="insertion" type="end"></suggestion>';
        $text = mb_substr($text, 0, $start) . mb_substr($text, mb_strpos($text, $tag) + mb_strlen($tag));
      }

      $result->setProcessedText($text);
    }

    return $result;
  }

}
