<?php

namespace Drupal\social_realtime_collaboration\Plugin\Filter;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\social_realtime_collaboration\Service\SocialRealtimeCollaborationHelperInterface;

/**
 * Defines an interface for a filter to restrict images to the site.
 *
 * @package Drupal\social_realtime_collaboration\Plugin\Filter
 */
interface SocialRealtimeCollaborationImageInterface extends ContainerFactoryPluginInterface {

  /**
   * Constructs a SocialRealtimeCollaborationSuggestion object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\social_realtime_collaboration\Service\SocialRealtimeCollaborationHelperInterface $helper
   *   The helper.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    SocialRealtimeCollaborationHelperInterface $helper
  );

}
