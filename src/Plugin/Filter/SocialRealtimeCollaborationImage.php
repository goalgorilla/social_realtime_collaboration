<?php

namespace Drupal\social_realtime_collaboration\Plugin\Filter;

use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\Filter\FilterHtmlImageSecure;
use Drupal\social_realtime_collaboration\Service\SocialRealtimeCollaborationHelperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a filter to restrict images to site.
 *
 * @Filter(
 *   id = "social_realtime_collaboration_image",
 *   title = @Translation("Restrict images to this site"),
 *   description = @Translation("Disallows usage of &lt;img&gt; tag sources that are not hosted on this site by replacing them with a placeholder image."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 *   weight = 9
 * )
 */
class SocialRealtimeCollaborationImage extends FilterHtmlImageSecure implements SocialRealtimeCollaborationImageInterface {

  /**
   * The helper.
   */
  protected SocialRealtimeCollaborationHelperInterface $helper;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    SocialRealtimeCollaborationHelperInterface $helper
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->helper = $helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('social_realtime_collaboration.helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode): FilterProcessResult {
    // Disable it since we use Easy Image integration for handling images in
    // CKEditor 5 and those files are not hosted on our server.
    if ($this->helper->isReady()) {
      return new FilterProcessResult($text);
    }

    return parent::process($text, $langcode);
  }

}
