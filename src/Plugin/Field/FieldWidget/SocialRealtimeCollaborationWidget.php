<?php

namespace Drupal\social_realtime_collaboration\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\text\Plugin\Field\FieldWidget\TextareaWithSummaryWidget;
use Drupal\url_embed\UrlEmbedHelperTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'social_realtime_collaboration_body' widget.
 *
 * @FieldWidget(
 *   id = "social_realtime_collaboration_body",
 *   label = @Translation("Text area with a summary"),
 *   field_types = {
 *     "text_with_summary"
 *   }
 * )
 */
class SocialRealtimeCollaborationWidget extends TextareaWithSummaryWidget {

  use UrlEmbedHelperTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->setUrlEmbed($container->get('url_embed'));
    return $instance;
  }

  /**
   * The CKEditor 4 markup for embed video.
   */
  const OLD_FORMAT = '<drupal-url data-embed-button="social_embed" data-embed-url="[url]" data-entity-label="Media" data-url-provider="[provider]"></drupal-url>';

  /**
   * The CKEditor 5 markup for embed video.
   */
  const NEW_FORMAT = '<figure class="media"><oembed url="[url]"></oembed></figure>';

  /**
   * The regular expression pattern dynamic parts.
   */
  const REPLACEMENTS = [
    '[url]' => '([^"]+)',
    '[provider]' => '[^"]+',
  ];

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $items[$delta]->value = preg_replace(
      '#' . strtr(self::OLD_FORMAT, self::REPLACEMENTS) . '#',
      str_replace('[url]', '$1', self::NEW_FORMAT),
      $items[$delta]->value
    );

    return parent::formElement($items, $delta, $element, $form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state): array {
    foreach ($values as &$item) {
      $matches = [];

      if (preg_match_all(
          '#' . str_replace('[url]', '([^"]+)', self::NEW_FORMAT) . '#',
          $item['value'],
          $matches
      )) {
        foreach ($matches[0] as $key => $match) {
          $replace_pairs = ['[url]' => $matches[1][$key]];

          try {
            $info = $this->urlEmbed()->getUrlInfo($matches[1][$key]);

            if (isset($info['providerName'])) {
              $replace_pairs['[provider]'] = $info['providerName'];
            }
          }
          catch (\Exception $exception) {
          }

          if (count($replace_pairs) === 1) {
            $replace_pairs['[provider]'] = 'Unknown';
          }

          $item['value'] = str_replace(
            $match,
            strtr(self::OLD_FORMAT, $replace_pairs),
            $item['value']
          );
        }
      }
    }

    return $values;
  }

}
