<?php

namespace Drupal\social_realtime_collaboration\Plugin\Editor;

use Drupal\ckeditor\CKEditorPluginManager;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\social_realtime_collaboration\Service\SocialRealtimeCollaborationHelperInterface;
use Drupal\Core\State\StateInterface;

/**
 * Defines an interface for overrides a CKEditor 5-based text editor for Drupal.
 *
 * @package Drupal\social_realtime_collaboration\Plugin\Editor
 */
interface SocialRealtimeCollaborationCKEditor5Interface {

  /**
   * Constructs a SocialRealtimeCollaborationCKEditor5 object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\ckeditor\CKEditorPluginManager $ckeditor_plugin_manager
   *   The CKEditor plugin manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke hooks on.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state key/value store.
   * @param \Drupal\social_realtime_collaboration\Service\SocialRealtimeCollaborationHelperInterface $helper
   *   The helper.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    CKEditorPluginManager $ckeditor_plugin_manager,
    ModuleHandlerInterface $module_handler,
    LanguageManagerInterface $language_manager,
    RendererInterface $renderer,
    StateInterface $state,
    SocialRealtimeCollaborationHelperInterface $helper,
    ConfigFactoryInterface $config_factory
  );

}
