<?php

namespace Drupal\social_realtime_collaboration\Plugin\Editor;

use Drupal\ckeditor\CKEditorPluginManager;
use Drupal\ckeditor\Plugin\Editor\CKEditor;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\State\StateInterface;
use Drupal\editor\Entity\Editor;
use Drupal\social_realtime_collaboration\Service\SocialRealtimeCollaborationHelperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Overrides a CKEditor 5-based text editor for Drupal.
 *
 * @Editor(
 *   id = "ckeditor5",
 *   label = @Translation("CKEditor 5"),
 *   supports_content_filtering = TRUE,
 *   supports_inline_editing = FALSE,
 *   is_xss_safe = TRUE,
 *   supported_element_types = {
 *     "textarea"
 *   }
 * )
 *
 * @package Drupal\social_realtime_collaboration\Plugin\Editor
 */
class SocialRealtimeCollaborationCKEditor5 extends CKEditor implements SocialRealtimeCollaborationCKEditor5Interface {

  /**
   * Convert button names from CKEditor 4 (keys) to CKEditor 5 (values).
   *
   * @var array
   */
  const BUTTONS = [
    '-' => '|',
    // Formatting.
    'Bold' => NULL,
    'Italic' => NULL,
    'Strike' => 'strikethrough',
    'Superscript' => NULL,
    'Subscript' => NULL,
    'RemoveFormat' => NULL,
    // Alignment.
    'JustifyLeft' => 'alignment:left',
    'JustifyCenter' => 'alignment:center',
    'JustifyRight' => 'alignment:right',
    'JustifyBlock' => 'alignment:justify',
    // Linking.
    'DrupalLink' => 'link',
    // Lists.
    'BulletedList' => NULL,
    'NumberedList' => NULL,
    // Media.
    'Blockquote' => NULL,
    'DrupalImage' => 'imageUpload',
    'Table' => 'insertTable',
    'HorizontalRule' => 'horizontalline',
    // Block Formatting.
    'Format' => 'heading',
    // Embed.
    'social_embed' => 'mediaEmbed',
  ];

  /**
   * The helper.
   */
  protected SocialRealtimeCollaborationHelperInterface $helper;

  /**
   * The config factory.
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    CKEditorPluginManager $ckeditor_plugin_manager,
    ModuleHandlerInterface $module_handler,
    LanguageManagerInterface $language_manager,
    RendererInterface $renderer,
    StateInterface $state,
    SocialRealtimeCollaborationHelperInterface $helper,
    ConfigFactoryInterface $config_factory
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $ckeditor_plugin_manager,
      $module_handler,
      $language_manager,
      $renderer,
      $state,
    );

    $this->helper = $helper;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.ckeditor.plugin'),
      $container->get('module_handler'),
      $container->get('language_manager'),
      $container->get('renderer'),
      $container->get('state'),
      $container->get('social_realtime_collaboration.helper'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor): array {
    $libraries = parent::getLibraries($editor);

    if (!$this->helper->isReady()) {
      return $libraries;
    }

    if ($this->helper->useCollaboration()) {
      $libraries[0] = 'social_realtime_collaboration/drupal.ckeditor5-with-collaboration';
    }
    else {
      $libraries[0] = 'social_realtime_collaboration/drupal.ckeditor5-without-collaboration';
    }

    return $libraries;
  }

  /**
   * {@inheritdoc}
   */
  public function buildToolbarJsSetting(Editor $editor): array {
    $old_toolbar = parent::buildToolbarJSSetting($editor);

    if (!(
      $this->helper->isReady() &&
      $editor->getEditor() === 'ckeditor5' &&
      (
        $editor->id() !== 'collaborative_full_html' ||
        $this->configFactory->getEditable('social_realtime_collaboration.settings')->get('override')
      )
    )) {
      return $old_toolbar;
    }

    $new_toolbar = [];
    $filled_row = FALSE;
    $first_row = $this->helper->useCollaboration();

    foreach ($old_toolbar as $toolbar_item) {
      if (is_array($toolbar_item)) {
        $items = [];

        foreach ($toolbar_item['items'] as $group_item) {
          if (array_key_exists($group_item, self::BUTTONS)) {
            $items[] = self::BUTTONS[$group_item] ?: $group_item;
          }
          elseif (in_array($group_item, self::BUTTONS)) {
            $items[] = $group_item;
          }
        }

        if ($items) {
          $toolbar_item['items'] = $items;
          $new_toolbar[] = $toolbar_item;
          $filled_row = TRUE;
        }
      }
      elseif ($filled_row) {
        if ($first_row) {
          $new_toolbar[] = [
            'name' => 'Track changes',
            'items' => [
              'comment',
              'trackChanges',
            ],
          ];

          $first_row = FALSE;
        }

        $new_toolbar[] = $toolbar_item;
        $filled_row = FALSE;
      }
    }

    return $new_toolbar;
  }

}
