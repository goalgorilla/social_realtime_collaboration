<?php

namespace Drupal\social_realtime_collaboration\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\social_realtime_collaboration\Service\SocialRealtimeCollaborationHelperInterface;

/**
 * Defines an interface for entity change validator.
 *
 * @package Drupal\social_realtime_collaboration\Plugin\Validation\Constraint
 */
interface SocialRealtimeCollaborationEntityChangedConstraintValidatorInterface extends ContainerInjectionInterface {

  /**
   * Constructs a RealtimeCollaborationEntityChangedConstraintValidator object.
   *
   * @param \Drupal\social_realtime_collaboration\Service\SocialRealtimeCollaborationHelperInterface $helper
   *   The helper.
   */
  public function __construct(SocialRealtimeCollaborationHelperInterface $helper);

}
