<?php

namespace Drupal\social_realtime_collaboration\Plugin\Validation\Constraint;

use Drupal\Core\Entity\Plugin\Validation\Constraint\EntityChangedConstraintValidator;
use Drupal\node\NodeInterface;
use Drupal\social_realtime_collaboration\Service\SocialRealtimeCollaborationHelperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;

/**
 * Validates the SocialRealtimeCollaborationEntityChanged constraint.
 *
 * @package Drupal\social_realtime_collaboration\Plugin\Validation\Constraint
 */
class SocialRealtimeCollaborationEntityChangedConstraintValidator extends EntityChangedConstraintValidator implements SocialRealtimeCollaborationEntityChangedConstraintValidatorInterface {

  /**
   * The helper.
   */
  protected SocialRealtimeCollaborationHelperInterface $helper;

  /**
   * {@inheritdoc}
   */
  public function __construct(SocialRealtimeCollaborationHelperInterface $helper) {
    $this->helper = $helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static($container->get('social_realtime_collaboration.helper'));
  }

  /**
   * {@inheritdoc}
   */
  public function validate($entity, Constraint $constraint): void {
    if (!(
      $entity instanceof NodeInterface &&
      !$entity->isNew() &&
      in_array($entity->bundle(), $this->helper->getContentTypes())
    )) {
      parent::validate($entity, $constraint);
    }
  }

}
