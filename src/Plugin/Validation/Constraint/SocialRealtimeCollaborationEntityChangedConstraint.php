<?php

namespace Drupal\social_realtime_collaboration\Plugin\Validation\Constraint;

use Drupal\Core\Entity\Plugin\Validation\Constraint\EntityChangedConstraint;

/**
 * Validation constraint for the entity changed timestamp.
 *
 * @package Drupal\social_realtime_collaboration\Plugin\Validation\Constraint
 */
class SocialRealtimeCollaborationEntityChangedConstraint extends EntityChangedConstraint {

}
