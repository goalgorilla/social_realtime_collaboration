<?php

namespace Drupal\social_realtime_collaboration\Service;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the helper service interface.
 */
interface SocialRealtimeCollaborationHelperInterface {

  /**
   * Returns setting.
   *
   * @param string $name
   *   The setting name.
   * @param string|null $default
   *   (optional) The default value to use if the key is not found.
   *
   * @return mixed
   *   The setting value.
   */
  public function get($name, $default = NULL);

  /**
   * Check if all settings have been set.
   */
  public function isReady(): bool;

  /**
   * Check access to the token endpoint.
   */
  public function checkAccess(): AccessResultInterface;

  /**
   * Check if fields on the current page should use collaboration plugin.
   */
  public function useCollaboration(): bool;

  /**
   * Check if fields on the current page should use CKEditor5.
   */
  public function useEditor5(): bool;

  /**
   * Returns the content types list which using the collaboration feature.
   *
   * @return array
   *   The machine names of content types.
   */
  public function getContentTypes(): array;

  /**
   * Returns list of editors based on CKEditor 5.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account.
   *
   * @return array
   *   The editor entity IDs.
   */
  public function getEditors(AccountInterface $account): array;

  /**
   * Limitate text formats on the idea edit page.
   *
   * @param array $element
   *   The form element to process. See main class documentation for properties.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The form element.
   */
  public static function processFormat(array &$element, FormStateInterface $form_state, array &$complete_form): array;

}
