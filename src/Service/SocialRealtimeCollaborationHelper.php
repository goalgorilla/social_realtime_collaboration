<?php

namespace Drupal\social_realtime_collaboration\Service;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\State\StateInterface;
use Drupal\node\NodeInterface;

/**
 * Defines the helper service.
 */
class SocialRealtimeCollaborationHelper implements SocialRealtimeCollaborationHelperInterface {

  /**
   * The state storage service.
   */
  protected StateInterface $state;

  /**
   * The currently active route match object.
   */
  protected RouteMatchInterface $routeMatch;

  /**
   * The entity type manager.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The configuration factory.
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * Constructs a new SocialRealtimeCollaborationHelper object.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state storage service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The currently active route match object.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct(
    StateInterface $state,
    RouteMatchInterface $route_match,
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory
  ) {
    $this->state = $state;
    $this->routeMatch = $route_match;
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function get($name, $default = NULL) {
    return $this->state->get('social_realtime_collaboration.' . $name, $default);
  }

  /**
   * {@inheritdoc}
   */
  public function isReady(): bool {
    $environment = $this->get('environment');
    return !empty($environment);
  }

  /**
   * {@inheritdoc}
   */
  public function checkAccess(): AccessResultInterface {
    return AccessResult::allowedIf($this->isReady());
  }

  /**
   * {@inheritdoc}
   */
  public function useCollaboration(): bool {
    $allowed_routes = [
      'quickedit.field_form',
      'entity.node.edit_form',
      'entity.node.canonical',
      'entity.node.collaboration',
    ];

    if (in_array($this->routeMatch->getRouteName(), $allowed_routes, TRUE)) {
      /** @var \Drupal\node\NodeInterface $node */
      $node = $this->routeMatch->getParameter('node');

      if (!$node instanceof NodeInterface) {
        $node = $this->routeMatch->getParameter('entity');
      }

      if (in_array($node->bundle(), $this->getContentTypes())) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function useEditor5(): bool {
    if ($this->useCollaboration()) {
      return TRUE;
    }

    $allowed_routes = [
      'entity.group_content.create_form',
      'node.add',
    ];

    if (in_array($this->routeMatch->getRouteName(), $allowed_routes, TRUE)) {
      // Group content plugin create page.
      $node_type = str_replace('group_node:', '', $this->routeMatch->getParameter('plugin_id'));
      if (!empty($node_type) && in_array($node_type, $this->getContentTypes())) {
        return TRUE;
      }
      // Node content create page.
      $node_type = $this->routeMatch->getParameter('node_type');
      if (!empty($node_type) && in_array($node_type->id(), $this->getContentTypes())) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getContentTypes(): array {
    return $this->configFactory->get('social_realtime_collaboration.settings')
      ->get('allowed_content_types') ?: [];
  }

  /**
   * {@inheritdoc}
   */
  public function getEditors(AccountInterface $account): array {
    $entity_query = $this->entityTypeManager->getStorage('editor')->getQuery();
    return (array) $entity_query
      ->accessCheck(TRUE)
      ->condition('editor', 'ckeditor5')
      ->condition('format', array_keys(filter_formats($account)), 'IN')
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public static function processFormat(array &$element, FormStateInterface $form_state, array &$complete_form): array {
    $allowed_forms = ['node_social_idea_edit_form'];

    foreach (\Drupal::moduleHandler()->getImplementations($hook = 'social_realtime_collaboration_forms') as $module) {
      $function = $module . '_' . $hook;

      if (is_callable($function)) {
        $allowed_forms = array_merge($allowed_forms, $function());
      }
    }

    if (!in_array($complete_form['#form_id'], $allowed_forms)) {
      return $element;
    }

    $formats = \Drupal::service('social_realtime_collaboration.helper')
      ->getEditors(\Drupal::currentUser());

    $element['#allowed_formats'] = array_merge(
      in_array($element['#format'], $formats) ? [] : $formats,
      [$element['#format']]
    );

    return $element;
  }

}
